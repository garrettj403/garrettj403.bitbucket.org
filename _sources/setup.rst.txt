Getting Started
===============

Downloading
-----------

You can either `download`_ or `clone`_ the QMix repository from `BitBucket`_. To clone the repository, use:

.. _download: https://bitbucket.org/garrettj403/qmix/downloads/

.. _clone: https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html

.. _BitBucket: https://bitbucket.org/garrettj403/qmix/

.. code-block:: bash
   
   git clone https://garrettj403@bitbucket.org/garrettj403/qmix.git

.. warning::

   The QMix package is currently private. You can still get private access if you email me (john.garrett@astro.ox.ac.uk), but access is currently limited to those in my group.

Setup
-----

Since the QMix package is still under constant development and no stable releases have been issued, it isn't worth installing it on your computer (e.g., through a setup.py file). Instead, the QMix/ directory needs to be added to your PYTHONPATH environment variable.

For Mac OS X
^^^^^^^^^^^^

A Makefile is included to make this as easy as possible:

   1. In the terminal, navigate to the QMix directory.

   2. Run ``make``. This will: 

      - add the QMix/ directory to your PYTHONPATH environment variable
      
      - upgrade the required Python packages

      - run the test suite

That's it!

**Manual Installation**

If this doesn't work, you would like to do this manually, or you don't have the permission to run bash scripts, follow these steps:

1. Open ``~/.bash_profile``:

.. code-block:: bash
   
   open -e ~/.bash_profile

2. Add the following to the end of the file:

.. code-block:: bash
   
   export PYTHONPATH="$PYTHONPATH:<path_to_qmix_on_your_computer>"

3. Save and make sure that the path has been updated.

.. code-block:: bash

   source ~/.bash_profile
   echo $PYTHONPATH

4. Install the required Python packages (listed in requirements.txt):

.. code-block:: bash

   pip install -r requirements.txt

5. Run the test suite

.. code-block:: bash

   pytest tests/

For Linux
^^^^^^^^^

Follow the instructions for Mac OS X, but edit ``.bashrc`` instead of ``.bash_profile``.

For Windows
^^^^^^^^^^^

Open:

   My Computer > Properties > Advanced System Settings > Environment Variables >

And then add the ``QMix/`` directory to the PYTHONPATH variable. More detailed instructions can be found `here`_.

.. _here: http://stackoverflow.com/questions/3701646/how-to-add-to-the-pythonpath-in-windows-7

Usage and Examples
------------------

The ``QMix/qmix/`` directory contains all of the Python modules, but to actually run anything meaningful, you'll need to create a workflow script.

You can find example workflows in::

   QMix/qmix/workflows/examples/

.. raw:: latex

    \newpage
