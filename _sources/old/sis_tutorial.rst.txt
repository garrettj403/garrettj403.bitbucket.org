SIS Simulation Guidelines
=========================

The ``QMix/qmix/`` directory contains all of the Python modules, but to actually run anything meaningful, you'll need to either create a workflow or open the GUI that I created. The workflows are much more powerful, but for quick testing, the GUI can be quite useful.

Running the Workflows
---------------------

To get the full power of the SIS mixer simulation code, you need to run it from a "workflow" script. Example workflows for SIS simulation are located in::

   QMix/qmix/workflows/sis_sim/examples/

The best way to learn how to make your own workflow is to read through these examples (there are lots of comments to help you along the way) and then try to adapt one of them for your own needs.

**The general process is:**

1. Define junction properties (gap voltage, normal resistance, etc.)
2. Define circuit parameters (frequencies, signal strengths, embedding impdances, etc.)
3. Define simulation parameters (number of Bessel functions to include, bias voltage sweep, accuracy of harmonic balance, etc.)
4. Load the desired response function (either from a model or from experimental data)
5. Perform harmonic balance to get the junction voltage for each tone/harmonic/bias voltage
6. Calculate the desired tunnelling currents
7. Whatever else post-processing you desire. E.g.,

   - plotting dc/ac tunnelling currents
   - calculate IF power
   - calculate gain
   - calculate linear circuit properties (i.e., from Tucker paper):

      - e.g., noise temperature

Using the GUI
-------------

.. note::
   I don't like the GUI. Tkinter is extremely tempermental. Creating a nice GUI is tedious. It's very rigid (can't adapt for special needs).

   Due to this, I haven't been working on this very much. The documentation could be out of date. This will likely be removed in future versions.

Using the GUI is very straight forward. Open your terminal and navigate to the ``QMix/`` directory.

Then run::

   python start_simulation_app.py

You're given a few options within the GUI:

- **Characteristic I-V Curve:** The response function (i.e., I-V curve and corresponding Kramers-Kronig transform) to use during the simulation. Use the drop-down menu to select which one you would like to use. The 'ideal' options use Kennedy's polynomial I-V curve model. The 'experimental' options use my own experimentally measured I-V curves. You have the ability to plot the I-V curve and the Kramers-Kronig transform.

- **Device Parameters:** Define the gap voltage and normal resistance.

- **Simulation Parameters:** Define the number of frequencies (a.k.a., tones) and harmonics to use during the simulation. Below you have the ability to define the properties of the various tones/harmonics, but the application will only read in however many you define here.

- **Voltage Sweep:** Define bias voltage sweep. Note that the bias voltage is normalized to the gap voltage.

- **Data Input:** This is where you define the properties of the input signals. Note that the 1st tone is the local-oscillator (LO), the 2nd tone is the signal-frequency (SF), and the 3rd is the intermediate-frequency (IF). 

   - **Frequency:** Signal frequency in GHz.
   - **Bessel Order:** When calculating the convolution coefficients, the number of coefficients must be truncated. This is the number of Bessel functions that will be considered. Typically 8 is sufficient for single-tone simulations, and 15 for multiple tones. The number can be lower if the signal strength is low.
   - **Thevenin Source Voltage:** The Thevenin source voltage (normalized to the gap voltage).
   - **Thevenin Impedance:** The embedding/Thevenin impedance (normalized to the normal resistance).

- **Updating and Plotting:**

   - **Update:** This will perform the harmonic balance and calculate the tunnelling current. This needs to be run if you change any of the parameters.
   - **Plot IF:** Plots the IF current.
   - **Plot RF:** Plots the I-V curve, the pumped I-V curve, and the AC tunnelling current of the local-oscillator.
   - **Quit:** Quit the application. You can also quit by going to the terminal (the one you started the GUI from) and typing control-c.

Note: Gain and noise temperature are not implemented yet, but I plan on doing this soon.

.. raw:: latex

    \newpage
    