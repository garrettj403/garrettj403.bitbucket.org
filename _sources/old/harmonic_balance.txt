Harmonic Balance Module -- :mod:`qmix.harmbal`
========================================================

.. currentmodule:: qmix.harmbal

.. automodule:: qmix.harmbal

All functionality may be found in the ``harmbal`` package::

   from qmix import harmbal 

Alternatively, import the main function directly::

   from qmix.harmbal import harmonic_balance

Functions
---------

.. automodule:: qmix.harmbal
   :members: